﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page4 : ContentPage
	{
        double firstNum = 0;
        double SecNum = 0;
        bool CalcReady = false;
        /// This eventMarker will be used to confirm what state the calculator for the numbers entered and the operators
        int eventMarker = 1;
        string mathOper;


        private string _displayNum;
        /// <summary>
        /// This DisplayNum function does the binding for the text in the label
        /// </summary>
        public string DisplayNum
        {
            get
            {
                return _displayNum;
            }
            set
            {
                _displayNum = value;
                OnPropertyChanged(nameof(DisplayNum));
            }
        }

        public Page4()
        {
            InitializeComponent();
            BindingContext = this;
            DisplayNum = "0";
        }

        void NumberEntered(object sender, EventArgs e)
        {
            double num;
            Button button = (Button)sender;
            string entered = button.Text;
            ///This first will check if there is a zero in the label and reset it to be empty
            if (this.DisplayNum == "0")
            {
                this.DisplayNum = "";
            }
            else if (eventMarker == -1)
            {///If the eventMaker is at -1 then reset label text and change eventMarker back to 1
                this.DisplayNum = "";
                eventMarker = 1;
            }
            else if (eventMarker == -2)
            {///If eventMarker is -2 then it will clear label text so its ready for the second number
                this.DisplayNum = "";
                eventMarker = 2;
            }
            ///This will add the new number to the label text so that it can do more
            ///than single digit numbers.
            this.DisplayNum += entered;
            ///Tryparse to make sure the parsing works correctly
            bool parseWorked = double.TryParse(this.DisplayNum, out num);
            if (parseWorked)
            {///add the complete number to the label
                this.DisplayNum = num.ToString();
                if (eventMarker == 1)
                {
                    firstNum = num;

                }
                else
                {
                    SecNum = num;
                    CalcReady = true;

                }
            }

        }
        void OperatorEntered(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            ///this section will be so that if the user enters 2 numbers already
            ///it will do the caculation if the user chooses to use another operation
            ///so if the user entered 2+4+3 it will do the calcuation of 2+4 first and on the second
            ///+ sign it will do the caculation before they enter the 3
            if (CalcReady)
            {
                var answer = MathDone(firstNum, SecNum, mathOper);
                this.DisplayNum = answer.ToString();
                mathOper = button.Text;
                firstNum = answer;
                eventMarker = -2;

                CalcReady = false;

            }
            ///This is happens when the first operator is entered
            else
            {
                mathOper = button.Text;
                eventMarker = -2;


            }

        }
        void ClearEntered(object sender, EventArgs e)
        {
            ///reset everything
            firstNum = 0;
            SecNum = 0;
            eventMarker = 1;
            this.DisplayNum = "0";
            CalcReady = false;


        }

        void CalculateEntered(object sender, EventArgs e)
        {
            ///Make sure that the calculator is in the correct state
            ///so that there are two numbers and a operator
            if (CalcReady)
            {
                var answer = MathDone(firstNum, SecNum, mathOper);
                this.DisplayNum = answer.ToString();
                eventMarker = -1;
                CalcReady = false;
                firstNum = answer;

            }
        }
        double MathDone(double first, double second, string Mathoperator)
        {
            double answer = 0;
            if (Mathoperator == "+")
            {
                answer = first + second;
            }
            else if (Mathoperator == "-")
            {
                answer = first - second;
            }
            else if (Mathoperator == "*")
            {
                answer = first * second;
            }
            else if (Mathoperator == "/")
            {
                answer = first / second;
            }
            return answer;
        }
        void Handle_disappearing(object sender, EventArgs e)
        {
            firstNum = 0;
            SecNum = 0;
            eventMarker = 1;
            this.DisplayNum = "0";
            CalcReady = false;
        }
    }
}
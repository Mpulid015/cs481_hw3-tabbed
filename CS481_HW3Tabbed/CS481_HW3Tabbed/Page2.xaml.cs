﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{   
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
        int AlbumState = 0;
		public Page2 ()
		{
			InitializeComponent ();
		}
        //Shows the display alert when the user goes to the page
        void Handle_appearing(object sender, EventArgs e)
        {
            DisplayAlert("Welcome", "This pages shows the first three albums of Metallica", "ok");
            Album.Source = "MetallicaKill.jpg";
            AlbumDetail.Text = "Released 1982\nList of Songs\nHit The Lights\nThe Four Horse Men\n Motorbreath\nJump In The Fire\n(Anesthesia)--Pulling Teeth" +
                        "\nWhiplash/nPhantom Lord\nNo Remorse\n Seek & Destroy\nMetal Milita ";
        }
        void Handle_disappearing(object sender, EventArgs e)
        {
            //not sure what to do
        }
        //Each button will changing the binding context based on what button was pressed
        void RideButton(object sender, EventArgs e)
        {
            Album.Source = "Ride.png";
            AlbumDetail.Text = "Released 1984 \n List of songs \n Fight Fire With Fire \nRide The Lightning\nFor Whom The Bell Tolls\nFade To Black" +
                "\nTrapped Under Ice \nEscape \nCreeping Death \nThe Call Of Ktulu";
            AlbumState = 1;
        }
        void Masterbutton(object sender, EventArgs e)
        {
            Album.Source = "MetallicaMaster.jpg";
            AlbumDetail.Text = "Released 1986\nList of Songs\nBattery\nMaster of Puppets\nThe Thing That Should Not Be" +
                        "\nWelcome Home\nDisposable Heroes\nLeper Messiah\nOrionDamage,Inc.";
            AlbumState = 2;
        }
        void KillButton(object sender, EventArgs e)
        {
            Album.Source = "MetallicaKill.jpg";
            AlbumDetail.Text = "Released 1982\nList of Songs\nHit The Lights\nThe Four Horse Men\n Motorbreath\nJump In The Fire\n(Anesthesia)--Pulling Teeth" +
                        "\nWhiplash/nPhantom Lord\nNo Remorse\n Seek & Destroy\nMetal Milita ";
            AlbumState = 0;
        }
        //Uses the state of the change what url is opened 
        //States changes when the button above are clicked
        void SendToWebsite(object sender,EventArgs e)
        {
            if (AlbumState == 0)
            {
                Device.OpenUri(new Uri("https://en.wikipedia.org/wiki/Kill_%27Em_All"));
            }
            if (AlbumState == 1)
            {
                Device.OpenUri(new Uri("https://en.wikipedia.org/wiki/Ride_the_Lightning"));
            }
            if (AlbumState == 2)
            {
                Device.OpenUri(new Uri("https://en.wikipedia.org/wiki/Master_of_Puppets"));
            }
        }
    }
}
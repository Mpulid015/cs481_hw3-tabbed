﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3extra : ContentPage
	{
		public Page3extra ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		}
        async void GoHomeClick(object sender, EventArgs e)
        {
            await DisplayAlert("Congrats", "You got it right Now Lets go to the Main Page", "Ok");
            await Navigation.PopAsync();
        }
        void WrongAns(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackgroundColor = Xamarin.Forms.Color.Red;

        }

    }
}
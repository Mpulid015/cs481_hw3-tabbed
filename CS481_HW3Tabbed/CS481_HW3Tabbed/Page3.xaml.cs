﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent ();
            NavigationPage.SetHasNavigationBar(this, false);
		}
        //Shows a alert to the user when they go to the page
        void Handle_appearing(object sender, EventArgs e)
        {
            DisplayAlert("Welcome", "This is is a quiz on the Metallica albums", "Ok");
        }
        void Handle_disappearing(object sender, EventArgs e)
        {
            //not sure what to do
        }
        //When the correct answer is chosen it will display an alert and go to the next page
        async void ExtraPageclick(object sender, EventArgs e)
        {
            await DisplayAlert("Good Job", "Now on to the second question", "Ok");
            await Navigation.PushAsync(new Page3extra());
        }
        //Changes the color of the button if it is the wrong answer
        void WrongAns(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.BackgroundColor = Xamarin.Forms.Color.Red;

        }
    }
}
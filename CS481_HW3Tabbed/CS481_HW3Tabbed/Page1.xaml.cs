﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        //These are to save the states of each button
        int aaState = 0, abState = 0, acState = 0, baState = 0, bbState = 0, bcState = 0, caState = 0, cbState = 0, ccState = 0;
        public Page1()
        {
            InitializeComponent();
        }
        async void Handle_appearing(object sender, EventArgs e)
        {
            bool result = await DisplayAlert("Welcome", "You can play tic tac toe here \n Would you like to start over?", "Yes" ,"No");
            if(result)
            {
                aaState = abState = acState = baState = bbState = bcState =caState =cbState =ccState = 0;
                aa.Text = "";
                ab.Text = "";
                ac.Text = "";
                ba.Text = "";
                bb.Text = "";
                bc.Text = "";
                ca.Text = "";
                cb.Text = "";
                cc.Text = "";
            }
        }
        void Handle_disappearing(object sender, EventArgs e)
        {
            //not sure what to do
        }
        void AAchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (aaState == 0)
            {
                button.Text = "X";
                aaState = 1;

            }
            else if (aaState == 1)
            {
                button.Text = "O";
                aaState = 2;
            }
            else if (aaState == 2)
            {
                button.Text = "";
                aaState = 0;
            }

        }

        void ABchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (abState == 0)
            {
                button.Text = "X";
                abState = 1;
            }
            else if (abState == 1)
            {
                button.Text = "O";
                abState = 2;
            }
            else if (abState == 2)
            {
                button.Text = "";
                abState = 0;
            }

        }
        void ACchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (acState == 0)
            {
                button.Text = "X";
                acState = 1;
            }
            else if (acState == 1)
            {
                button.Text = "O";
                acState = 2;
            }
            else if (acState == 2)
            {
                button.Text = "";
                acState = 0;
            }

        }
        void BAchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (baState == 0)
            {
                button.Text = "X";
                baState = 1;
            }
            else if (baState == 1)
            {
                button.Text = "O";
                baState = 2;
            }
            else if (baState == 2)
            {
                button.Text = "";
                baState = 0;
            }

        }
        void BBchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (bbState == 0)
            {
                button.Text = "X";
                bbState = 1;
            }
            else if (bbState == 1)
            {
                button.Text = "O";
                bbState = 2;
            }
            else if (bbState == 2)
            {
                button.Text = "";
                bbState = 0;
            }

        }
        void BCchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (bcState == 0)
            {
                button.Text = "X";
                bcState = 1;
            }
            else if (bcState == 1)
            {
                button.Text = "O";
                bcState = 2;
            }
            else if (bcState == 2)
            {
                button.Text = "";
                bcState = 0;
            }

        }
        void CAchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (caState == 0)
            {
                button.Text = "X";
                caState = 1;
            }
            else if (caState == 1)
            {
                button.Text = "O";
                caState = 2;
            }
            else if (caState == 2)
            {
                button.Text = "";
                caState = 0;
            }

        }
        void CBchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (cbState == 0)
            {
                button.Text = "X";
                cbState = 1;
            }
            else if (cbState == 1)
            {
                button.Text = "O";
                cbState = 2;
            }
            else if (cbState == 2)
            {
                button.Text = "";
                cbState = 0;
            }

        }
        void CCchange(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (ccState == 0)
            {
                button.Text = "X";
                ccState = 1;
            }
            else if (ccState == 1)
            {
                button.Text = "O";
                ccState = 2;
            }
            else if (ccState == 2)
            {
                button.Text = "";
                ccState = 0;
            }
        }
    }
}